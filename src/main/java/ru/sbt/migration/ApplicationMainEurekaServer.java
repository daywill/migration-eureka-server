package ru.sbt.migration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ApplicationMainEurekaServer {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationMainEurekaServer.class, args);
    }
}
