package ru.sbt.migration.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mainPage")
public class MainPageController {

    @GetMapping(value = "/mainPage")
    public String mainPageController() {
        return "Hello";
    }
}
