package ru.sbt.migration.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.sbt.migration")
public class ConfigEurekaServer {

}
